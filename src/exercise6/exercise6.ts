export type Action = {
  canRunAfterDelay: number;
  mustRunBeforeDelay: number;
  command: string;
};

/** Determine all actions that can run at the time the first action must run, and when that is */
export const determineWhenToRun = (
  actions: Action[],
): null | { delay: number; actions: Action[] } => {
  throw new Error('Not implemented');
};
