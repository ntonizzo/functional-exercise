export type Tree =
  | number /* literal value */
  | string /* variable reference */
  | {
    call: string,
    args: Tree[]
  };

export type FunctionEnvironment = Record<string, (args: number[]) => number>

export type ValueEnvironment = Record<string, number>